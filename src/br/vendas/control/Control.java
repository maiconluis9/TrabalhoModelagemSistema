package br.vendas.control;

import java.util.ArrayList;
import java.util.List;

import br.vendas.modelo.Cliente;
import br.vendas.modelo.Compra;
import br.vendas.modelo.Endereco;
import br.vendas.modelo.ItemCompra;
import br.vendas.modelo.Produto;

public class Control {
	
	private static Control controle =null;
	private Cliente clienteAtual=null;
	private List<Produto> cadastrarProduto = new ArrayList<Produto>();
	private List<Cliente> cadastrarCliente = new ArrayList<Cliente>();
	
	
	private Control(){}
	
	public static Control getInstance(){
		if(controle == null){
			return controle;
		} else throw new RuntimeException("Criado com sucesso!!");
	}
	
	
	public void cadastrarProduto(int codigo,String descricaoProduto, int quantidade, double preco){
		Produto pro = new Produto(codigo,descricaoProduto,quantidade,preco);
		boolean achoProduto = false;
		for(Produto produto: cadastrarProduto){
				if(produto.getDescricaoProduto().equals(descricaoProduto)){
					achoProduto = true;
					break;
				}
		}
		if(! achoProduto){
			cadastrarProduto.add(pro);
		}
	}
	
	
	public List<String> selecionarProdutosCadastrado(){
		List<String> produtos = new ArrayList<String>();
		for(Produto produto:cadastrarProduto){
			produtos.add(produto.getDescricaoProduto());
		}
		return produtos;
	}
	
	public void cadastrarCliente(int cpf, String nome, String telefone, String dataCadastro, Endereco tipo ){
		Cliente clie = new Cliente(cpf,nome,telefone,dataCadastro, tipo);
		boolean achouCliente = false;
		for(Cliente cliente: cadastrarCliente){
			if (cliente.getNome().equals(nome)){
				achouCliente = true;
				break;
			}
			
		}
		if(! achouCliente){
			cadastrarCliente.add(clie);
		}		
	}
	
	public List<String> selecionarClientesCadastrado (){
		List<String> clientes = new ArrayList<String>();
		for(Cliente cliente: cadastrarCliente){
			clientes.add(cliente.getNome());
		}
		return clientes;
	}
	
	public void abrirCompra(int quantidade){
		Compra comp = new Compra(quantidade);
		if(clienteAtual != null){
			clienteAtual.adicionarCompra(comp);
		}else throw new RuntimeException("Cliente n�o selecionado!!");
	}
	
	public void selecionarClienteNome(String nome) {
		for(Cliente clie: cadastrarCliente) {
			if(clie.getNome().equals(nome)) {
				clienteAtual=clie;
			}
		}
	}
	
	public void venderProduto(int numero,int codigo,String descricaoProduto, int quantidade) {
		if(clienteAtual != null) {
			Compra compraAtual = clienteAtual.pegarCompraPorNumero(numero);
			if(compraAtual !=null) {
				for(Produto produto: cadastrarProduto) {
					if(produto.getDescricaoProduto().equals(descricaoProduto)) {
						compraAtual.addItemCompra(new ItemCompra(descricaoProduto,quantidade, codigo, numero));
					}
				}
			}else  throw new RuntimeException("Compra n�o foi aberta!!");
		}else throw new RuntimeException("Cliente n�o foi selecionado!!");
	}
	
	public double totalizarVenda(int numeroCompra){
		double total=0;
		if(clienteAtual != null){
			Compra compraAtual = clienteAtual.pegarCompraPorNumero(numeroCompra);
			total = compraAtual.itensTotal();
		}	else throw new RuntimeException("Compra n�o foi aberta!!");
	return total;
}
}