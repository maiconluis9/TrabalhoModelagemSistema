package br.vendas.modelo;

import java.util.ArrayList;
import java.util.List;

public class Compra {
	private int quantidadeComprada;
	private int numero;
	
	private List<ItemCompra> itens = new ArrayList<ItemCompra>();	
	
	
	public Compra(){
		
	}
	
	public Compra (int pQuantidadeComprada){ 
		this.quantidadeComprada = pQuantidadeComprada;
	}

	
	public int getQuantidadeComprada() {
		return quantidadeComprada;
	}

	public void setQuantidadeComprada(int quantidadeComprada) {
		this.quantidadeComprada = quantidadeComprada;
	}

	
	public void addItemCompra(ItemCompra item){
		itens.add(item);
	}
	
	
	public double itensTotal(){
		double total=0;
			for(ItemCompra item:itens){
				total = total + item.getQuantidade()*item.getProduto().getPreco();					
			}
			return total;
	}
	
	public List<ItemCompra> getItens(){
		return itens;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	

}
